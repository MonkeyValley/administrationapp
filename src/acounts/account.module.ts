import { Module } from '@nestjs/common';
import { AccountService } from './acount.service';
import { AccountController } from './acount.controller';
import { MongooseModule } from '@nestjs/mongoose'
import { AccountSchema } from './schema/account.schemas';

const SchemasModules = MongooseModule.forFeature([
  {name:'Account', schema: AccountSchema}
])
@Module({
  providers: [AccountService],
  imports:[SchemasModules],
  controllers:[AccountController],
  exports:[AccountService],
})
export class AccountsModule {}
