import { Body, Controller, Delete, Get, Param, Put, Post, UseGuards } from '@nestjs/common';
import { Account } from './interfaces/Account';
import { AccountService } from './acount.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CreateAccountDto } from './dto/CreateAccountDto';

@UseGuards(JwtAuthGuard)
@Controller('account')
export class AccountController {
    constructor(private accountService:AccountService){}
    @Get()
    getAccounts():{}{
        return this.accountService.getAccounts();
    }

    @Get(':id')
    getAccount(@Param('id') id):{}{
        return this.accountService.getAccount(id);
    }

    @Get('byUser/:id')
    getAccountsByUser(@Param('id') id):{}{
        return this.accountService.getAccountsByUser(id);
    }

    @Post('create')
    createAccount(@Body() data:Account):{}{
        return this.accountService.createAccount(data);
    }

    @Delete(':id')
    deleteAccount(@Param('id') id):{}{
        return this.accountService.deleteAccount(id);
    }

    @Put(':id')
    updateAccount(@Param('id') id, @Body() balance:CreateAccountDto):{}{
        return this.accountService.updateAccount(balance, id);
    }
}
