import { ConsoleLogger, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Account } from './interfaces/Account';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateAccountDto } from './dto/CreateAccountDto';

@Injectable()
export class AccountService {

    constructor(@InjectModel('Account') private accountsModel:Model<Account>){}

    async getAccounts(): Promise<{}> {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/");
        console.log("data --> NA");

        try {
            const response = await this.accountsModel.find();
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getAccount(id:string): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/:id");
        console.log("data --> ", id);

        try {
            const response = await this.accountsModel.findById(id);
            console.log("response->", response)
            if(response && response != null){
                return {data:response, respuesta:true}
            }else{
                return {error:"Cuenta no existente", respuesta:false}
            } 
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getAccountsByUser(id:string): Promise<{}> {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/byUser");
        console.log("data --> ", id);

        try {
            const response = await this.accountsModel.find({idUser:id}).exec();
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async createAccount(item:Account): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/create");
        console.log("data --> ", item);

        if(!('name' in item) || item.name == "" ||!('idUser' in item) || item.idUser === "" || item.balance <= 0 || !("balance" in item)){
            throw new HttpException('Faltan algunos datos obligatorios, favor de verificar.', HttpStatus.BAD_REQUEST);
        }

        try {
            const newObject = new this.accountsModel(item);
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                return {data:response, respuesta:true}
            }else{
                throw new HttpException("Registro no creado", HttpStatus.UNPROCESSABLE_ENTITY);
            } 
        } catch (error) {
            console.log("Response --> ", error);
            throw new HttpException("Petición no válida. " + error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

    async deleteAccount(id:string):Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/delete");
        console.log("data --> ", id);

        if(!id || id === ""){
            return {error:"Faltan datos", respuesta:false}
        }
        try {
            const deleteItem = await this.accountsModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if(deleteItem && deleteItem != null){
                return {data:deleteItem, respuesta:true}
            }else{
                return {error:"Cuenta no válida", respuesta:false}
            }
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async updateAccount(data:CreateAccountDto, id:string): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("id --> ", id);
        console.log("data --> ", data);
        try{
            if(id){
                const account = await this.accountsModel.findById(id);
                if(account){
                    console.log("Cuenta --->", account)

                    if('balance' in data){ 
                        account.balance = data.balance;
                        const response = await account.save();
                        if(response){
                            return {respuesta:true, data:response}
                        }else{
                            return {respuesta:false, error:'No se pudo actualizar la cuenta'}
                        }
                    }else{
                        return {respuesta:false, error:'Dato [balance] es necesario.'}
                    }

                }else{
                    return {respuesta:false, error:'Cuenta no existente.'}
                }
            }
        }catch(err){
            return {respuesta:false, error:'Error inesperado. [2000] ' + err}
        }
    }
}
