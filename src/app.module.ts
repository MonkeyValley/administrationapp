import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RegistersModule } from './registers/registers.module';
import { UsersModule } from './users/users.module';
import { MethodsPayModule } from './methods-pay/methods-pay.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersRegistryModule } from './users.registry/users.registry.module';
import { AuthModule } from './auth/auth.module';
import { CategoryModule } from './categories/category.module';
import { StatusModule } from './status/status.module';
import { AccountsModule } from './acounts/account.module';
const MongooseConfig = {useNewUrlParser:true, };
const MongooseConectionModule = MongooseModule.forRoot('mongodb://localhost/administrationdb', MongooseConfig, );

@Module({
  imports: [MongooseConectionModule, RegistersModule, UsersModule, MethodsPayModule, UsersRegistryModule, AuthModule, CategoryModule, StatusModule, AccountsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
