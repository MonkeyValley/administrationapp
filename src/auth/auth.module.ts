import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { AuthService } from './services/auth.service';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthController } from './controllers/auth.controller';
import { UsersModule } from 'src/users/users.module';
import { SECRETKEY } from './../models/enviroment';

const ModuleJWTSchedule = JwtModule.register({secret: SECRETKEY, signOptions:{expiresIn:'365d'}})

@Module({
  imports:[
    PassportModule, 
    UsersModule, 
    ModuleJWTSchedule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
