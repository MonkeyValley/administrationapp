import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(private reflector:Reflector){}

  canActivate( context: ExecutionContext,): boolean | Promise<boolean> | Observable<boolean> {

    const isPublic = this.reflector.get('isPublic', context.getHandler());

    if(isPublic) return true

    const request = context.switchToHttp().getRequest<Request>();
    const authHeader = request.header('Authorization');
    const isAuth = authHeader === "123";
    if(!isAuth) throw new UnauthorizedException("No estas autorizado");
    return isAuth;
  }
}
