import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { MongooseModule } from '@nestjs/mongoose'
import { CategorySchema } from './schemas/category.schema';

const SchemasModules = MongooseModule.forFeature([
  {name:'Category', schema: CategorySchema}
])
@Module({
  providers: [CategoryService],
  imports:[SchemasModules],
  controllers:[CategoryController],
  exports:[CategoryService],
})
export class CategoryModule {}
