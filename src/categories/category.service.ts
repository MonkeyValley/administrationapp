import { Injectable } from '@nestjs/common';
import { Category } from './interfaces/Category';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCategoryDto } from './dto/CreateCategoryDto';

@Injectable()
export class CategoryService {
    constructor(@InjectModel('Category') private categoryModel:Model<Category>){}

    async getCategories(): Promise<{}> {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/");
        console.log("data --> NA");

        try {
            const response = await this.categoryModel.find();
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
    async createCategory(item:Category): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/create");
        console.log("data --> ", item);

        if(!('description' in item) || item.description === ""){
            return {respuesta:false, error:"faltan parametros"}
        }

        try {
            const newObject = new this.categoryModel(item);
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                return {data:response, respuesta:true}
            }else{
                return {error:"Categoria no creada", respuesta:false}
            } 
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
        
    }

    async deleteCategory(id:string):Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/update");
        console.log("data --> ", id);

        if(!id || id === ""){
            return {error:"Faltan datos", respuesta:false}
        }
        try {
            const deleteItem = await this.categoryModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if(deleteItem && deleteItem != null){
                return {data:deleteItem, respuesta:true}
            }else{
                return {error:"Registro no válido", respuesta:false}
            }
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
}
