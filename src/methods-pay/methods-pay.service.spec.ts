import { Test, TestingModule } from '@nestjs/testing';
import { MethodsPayService } from './methods-pay.service';

describe('MethodsPayService', () => {
  let service: MethodsPayService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MethodsPayService],
    }).compile();

    service = module.get<MethodsPayService>(MethodsPayService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
