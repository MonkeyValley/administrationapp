import { Injectable } from '@nestjs/common';
import { MethodPay } from './interfaces/MethodPay';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class MethodsPayService {

    constructor(@InjectModel('MethosPay') private methosPayModel:Model<MethodPay>){}

    async getMethodsPay(): Promise<{}> {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /methos-pay/");
        console.log("data --> NA");

        try {
            const response = await this.methosPayModel.find();
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getMethodPay(id:string): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /methos-pay/:id");
        console.log("data --> ", id);

        try {
            const response = await this.methosPayModel.findById(id);
            console.log("response->", response)
            if(response && response != null){
                return {data:response, respuesta:true}
            }else{
                return {error:"Metodo de pago no existente", respuesta:false}
            } 
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
    async createMethodPay(item:MethodPay): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/create");
        console.log("data --> ", item);

        if(!('description' in item) || item.description === ""){
            return {respuesta:false, error:"faltan parametros"}
        }

        try {
            const newObject = new this.methosPayModel(item);
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                return {data:response, respuesta:true}
            }else{
                return {error:"Metodo de pago no creado", respuesta:false}
            } 
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
        
    }

    async deleteMethodPay(id:string):Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/update");
        console.log("data --> ", id);

        if(!id || id === ""){
            return {error:"Faltan datos", respuesta:false}
        }
        try {
            const deleteItem = await this.methosPayModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if(deleteItem && deleteItem != null){
                return {data:deleteItem, respuesta:true}
            }else{
                return {error:"Registro no válido", respuesta:false}
            }
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
}
