import { ConsoleLogger, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {Register} from './interfaces/Register'
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateRegisterDto } from './dto/CreateRegisterDto';
import { CategoryService } from 'src/categories/category.service';
import { Category } from 'src/categories/interfaces/Category';
import { AccountService } from 'src/acounts/acount.service';
import { Account } from 'src/acounts/interfaces/Account';
import { CreateAccountDto } from 'src/acounts/dto/CreateAccountDto';
import { parse } from 'path';
import * as d3 from 'd3-collection';
let mongooe = require('mongoose');

@Injectable()
export class RegistersService {

    constructor(@InjectModel('Register') private registerModel:Model<Register>, 
    private categoryServices:CategoryService,
    private accountServices:AccountService){}

    async getRegisters(): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/");
        console.log("data --> NA");

        try {
            const response = await this.registerModel.find();
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getRegister(id): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/:id");
        console.log("data --> ", id);

        try {
            const response = await this.registerModel.findById(id);
            console.log("response->", response)
            if(response && response != null){
                return {data:response, respuesta:true}
            }else{
                return {error:"Registro no existente", respuesta:false}
            } 
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getAnalithyscsByAccount(id, filter): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /getAnalithyscsByAccount/:id");
        console.log("data --> ", id);
        console.log('filter -->', filter)
       
        try { 
            let dte = this.getStrDate(new Date());
            let startToendMonth = this.getDateMonth(dte);
            const response = await this.registerModel.aggregate([
                { $addFields: {stringDate: { $dateToString: { format: "%Y-%m-%d", date: "$createAt" } } } },
                { $match:{idAccount:mongooe.Types.ObjectId(id), "stringDate":startToendMonth}},    
            ])
            
            var expensesByDate = d3.nest()
            .key(function(d) { return d.idCategory}).sortKeys(d3.ascending)
            .entries(response); 
            return {data:expensesByDate,  respuesta:true}
        } catch (error) {
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    async getRegistersByAccount(id): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /getRegisterById/:id");
        console.log("data --> ", id);
        try { 
            let dte = this.getStrDate(new Date());
            let startToendMonth = this.getDateMonth(dte);
            const response = await this.registerModel.aggregate([
                { $addFields: {stringDate: { $dateToString: { format: "%Y-%m-%d", date: "$createAt" } } } },
                { $match:{idAccount:mongooe.Types.ObjectId(id), "stringDate":startToendMonth}},    
            ])
            console.log(startToendMonth)
            var expensesByDate = d3.nest()
            .key(function(d) { return d.createAt}).sortKeys(d3.ascending)
            .entries(response); 
            let arrayinverted = expensesByDate.sort(this.byDate)
            return {data:arrayinverted,  respuesta:true}
        } catch (error) {
            throw new HttpException("Petición no válida. " + error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async createRegister(data:CreateRegisterDto): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/create");
        console.log("data --> ", data);

        if(!('price' in data) ||  !('idCategory' in data) ||  !('idAccount' in data) ||  !('name' in data) ||
        data.price < 0 || data.name == ""){
            throw new HttpException('Faltan algunos datos obligatorios, favor de verificar.', HttpStatus.BAD_REQUEST);
        }

        try {
            const newObject = new this.registerModel(data);
            const response = await newObject.save();

            if(response){
                const responseAccount = await this.accountServices.getAccount(response.idAccount) as { respuesta: Boolean, error?: String, data?:Account }
                const account = responseAccount.data
                var formula = account.balance 
                if(response.typeMovement == 1){
                    formula = account.balance - (response.price * response.quantity)
                }else{
                    formula = account.balance + (response.price * response.quantity)
                }
                account.balance = formula
                const accountEdit = await this.accountServices.updateAccount(account, response.idAccount) as { respuesta: Boolean, error?: String, data?:Account }
                if(accountEdit.respuesta){
                    return {data:response, respuesta:true}
                }else{
                    this.deleteRegister(response.id)
                    throw new HttpException("Registro no creado" + accountEdit.error, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            }else{
                throw new HttpException("Registro no creado", HttpStatus.UNPROCESSABLE_ENTITY);
            } 
        } catch (error) {
            throw new HttpException("Petición no válida. " + error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async deleteRegister(id:string): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/update");
        console.log("data --> ", id);

        if(!id || id === ""){
            return {error:"Faltan datos", respuesta:false}
        }

        try {
            const deleteItem = await this.registerModel.findByIdAndDelete(id);
            if(deleteItem && deleteItem != null){
                return {data:deleteItem, respuesta:true}
            }else{
                return {error:"Registro no válido", respuesta:false}
            }
        } catch (error) {
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }

    getStrDate(dateToParse:Date):String{
        const formatYmd = date => date.toISOString().slice(0, 10);
        return formatYmd(dateToParse);
    }
    getDateMonth(formateDte:String):{}{
        const sptDate = formateDte.split("-");
        const complemention = sptDate[0] + "-" + sptDate[1];
        const startdate = complemention + "-01"
        const enddate = complemention + "-31"
        return {$gte:startdate, $lte:enddate}
    }

    byDate(a,b){
        return new Date(b.key).valueOf() - new Date(a.key).valueOf() 
    }
}


