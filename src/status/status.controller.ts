import { Controller, Body, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { StatusService } from './status.service';
import { Status } from './interfaces/Status';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('status')
export class StatusController {
    constructor(private statuasService:StatusService){}
    @Get()
    getMethodsPay():{}{
        return this.statuasService.getStatus();
    }

    @Post('create')
    createMethodPay(@Body() data:Status):{}{
        return this.statuasService.createStatus(data);
    }

    @Delete(':id')
    deleteMethodPay(@Param('id') id):{}{
        return this.statuasService.deleteStatus(id);
    }
}
