import { Module } from '@nestjs/common';
import { StatusService } from './status.service';
import { StatusController } from './status.controller';
import { MongooseModule } from '@nestjs/mongoose'
import { StatusSchema } from './schemas/status.schema';

const SchemasModules = MongooseModule.forFeature([
  {name:'Status', schema: StatusSchema}
])
@Module({
  providers: [StatusService],
  imports:[SchemasModules],
  controllers:[StatusController],
  exports:[StatusService]
})
export class StatusModule {}
