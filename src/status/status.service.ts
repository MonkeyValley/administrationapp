import { Injectable } from '@nestjs/common';
import { Status } from './interfaces/Status';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class StatusService {

    constructor(@InjectModel('Status') private statusModel:Model<Status>){}

    async getStatus(): Promise<{}> {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /status/");
        console.log("data --> NA");

        try {
            const response = await this.statusModel.find();
        
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
    async createStatus(item:Status): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /status/create");
        console.log("data --> ", item);

        if(!('description' in item) || item.description === ""){
            return {respuesta:false, error:"faltan parametros"}
        }

        try {
            const newObject = new this.statusModel(item);
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                return {data:response, respuesta:true}
            }else{
                return {error:"Categoria no creada", respuesta:false}
            } 
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
        
    }

    async deleteStatus(id:string):Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /status/update");
        console.log("data --> ", id);

        if(!id || id === ""){
            return {error:"Faltan datos", respuesta:false}
        }
        try {
            const deleteItem = await this.statusModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if(deleteItem && deleteItem != null){
                return {data:deleteItem, respuesta:true}
            }else{
                return {error:"Registro no válido", respuesta:false}
            }
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
}

