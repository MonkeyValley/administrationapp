import { Schema } from "mongoose";

export const UserRegistrySchema = new Schema({
    token:String,
	idUser: Number,
    isActive:Boolean,
    count:Number,
	createAt:{
		type:Date,
		default: Date.now
	},
	updateAt:{
		type:Date,
		default: Date.now
	}
})
