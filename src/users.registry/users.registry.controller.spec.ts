import { Test, TestingModule } from '@nestjs/testing';
import { Users.RegistryController } from './users.registry.controller';

describe('Users.RegistryController', () => {
  let controller: Users.RegistryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Users.RegistryController],
    }).compile();

    controller = module.get<Users.RegistryController>(Users.RegistryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
