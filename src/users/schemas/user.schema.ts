import { Schema } from "mongoose";

export const UserSchema = new Schema({
    email: String,
	type: Number,
    password: String,
	name: String,
	phone: String,
	employeeCode: String,
	bussinesUnityId: String,
	status: Number,
	createAt:{
		type:Date,
		default: Date.now
	},
	updateAt:{
		type:Date,
		default: Date.now
	}
})

