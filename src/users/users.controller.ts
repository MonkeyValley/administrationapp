import { Body, Controller, Get, Param, Put, Post, UseGuards, Delete} from '@nestjs/common';

import { CreateUserDto } from './dto/CreateUserDto';
import { UsersService } from './users.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Public } from 'src/auth/decorators/public.decorator';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { Role } from 'src/models/rol.model';
import { RolesAuthGuard } from 'src/auth/guards/roles-auth.guard';

@UseGuards(JwtAuthGuard, RolesAuthGuard)
@Controller('users')
export class UsersController {

    constructor(
        private usersService:UsersService,
        ){}
   
    @Roles(Role.ADMIN)
    @Get()
    getUsers():{}{
        return this.usersService.getUsers();
    }

    @Roles(Role.ADMIN)
    @Get(':id')
    getUser(@Param('id') id): {}{
        return this.usersService.getUser(id);
    }
    
    @Public()
    @Post('create')
    createUser(@Body() data:CreateUserDto):{}{
        return this.usersService.createUser(data);
    }

    @Roles(Role.ADMIN)
    @Post('createAdmin')
    createAdmin(@Body() data:CreateUserDto):{}{
        return this.usersService.createAdmin(data);
    }

    @Roles(Role.ADMIN)
    @Put(':id')
    updateStatusUser(@Body() data:CreateUserDto, @Param('id') id):{}{
        return this.usersService.updateUser(data, id);
    }

    @Roles(Role.ADMIN)
    @Delete(':id')
    deleteUser(@Param('id') id):{}{
        return this.usersService.deleteUser(id);
    }
}
