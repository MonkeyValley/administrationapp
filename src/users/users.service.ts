import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt'

import {User} from './interfaces/User'
import { CreateUserDto } from './dto/CreateUserDto';
import { BaseUser } from 'src/models/base.user';

@Injectable()
export class UsersService {

    constructor(@InjectModel('User') private userModel:Model<User>){}

    async getUsers(): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/");
        console.log("data --> NA");

        try {
            const response = await this.userModel.find();
            response.forEach(it =>{
                it.password = undefined;
            })
            console.log("response --> ", response);
            return {data: response , respuesta:true};
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Petición no válida. " + error, respuesta:false}
        }
    }
    async getUser(id): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("data --> ", id);

        try {
            const response = await this.userModel.findById(id);
            console.log("response->", response)
            if(response && response != null){
                const {password, ...rts} = response.toJSON();
                return {data:rts, respuesta:true}
            }else{
                return {error:"Usuario no registrado", respuesta:false}
            } 
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Usuario no válido. " + error, respuesta:false}
        }
        
    }

    async getUserByMail(email:string): Promise<{respuesta:boolean, data?:User, error?:string}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/getUserByMail");
        console.log("data --> ", email);

        try {
            const response = await this.userModel.findOne({email:email}).exec();
            console.log("response->", response)
            if(response && response != null){
                return {data:response , respuesta:true}
            }else{
                return {error:"Usuario no registrado", respuesta:false}
            } 
        } catch (error) {
            console.log("response --> ", error);
            return {error:"Usuario no válido. " + error, respuesta:false}
        }
        
    }
    
    async createUser(data:CreateUserDto): Promise<{}>{

        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/Create");
        console.log("data --> ", data );

        try {

            if(!('email' in data) ||  !('password' in data)||  !('name' in data)|| !('phone' in data) ||
            data.email == "" || data.password == "" || data.name == "" || data.phone == ""){
                return {error:"Faltan datos", respuesta:false}
            }

            const isUserExist =  await this.getUserByMail(data.email) as BaseUser; 
            console.log("response getByEmail->", isUserExist.respuesta)

            if(isUserExist.respuesta) return {error:"Ya existe un usuario con ese correo", respuesta:false}

            const bCryptPass = await bcrypt.hash(data.password, 10);
            const newObject = new this.userModel(data);
            newObject.password = bCryptPass;
            newObject.status = 1;
            newObject.type = 1;
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                const {password, ...rta} = response.toJSON();
                return {data:rta, respuesta:true}
            }else{
                return {error:"Usuario no registrado", respuesta:false}
            } 
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Usuario no válido. " + error, respuesta:false}
        }
        
    }

   async deleteUser(id:string):Promise<{}> {
    console.log("\n -------------------------------------------------");
    console.log("service -->  /users/delete");
    console.log("data --> ", id );

    try {

        if(!id && id == ""){ return {}}

        const query = await this.userModel.findByIdAndDelete(id);
        if(query){
            return {data:query, respuesta:true}
        }else{
            return {error:'Usuario no se pudo eliminar.', respuesta:false}
        }

    } catch (error) {
        console.log("Response --> ", error);
        return {error:"Usuario no válido. " + error, respuesta:false}
    }
   }

    async createAdmin(data:CreateUserDto): Promise<{}>{
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/Create");
        console.log("data --> ", data );

        try {

            if(!('email' in data) ||  !('password' in data)||  !('name' in data)|| !('phone' in data) ||
            data.email == "" || data.password == "" || data.name == "" || data.phone == ""){
                return {error:"Faltan datos", respuesta:false}
            }

            const isUserExist =  await this.getUserByMail(data.email) as BaseUser; 
            console.log("response getByEmail->", isUserExist.respuesta)

            if(isUserExist.respuesta) return {error:"Ya existe un usuario con ese correo", respuesta:false}

            const bCryptPass = await bcrypt.hash(data.password, 10);
            const newObject = new this.userModel(data);
            newObject.password = bCryptPass;
            newObject.type = 0;
            newObject.status = 1;
            const response = await newObject.save();

            console.log("response->", response)
            if(response){
                const {password, ...rta} = response.toJSON();
                return {data:rta, respuesta:true}
            }else{
                return {error:"Usuario no registrado", respuesta:false}
            } 
        } catch (error) {
            console.log("Response --> ", error);
            return {error:"Usuario no válido. " + error, respuesta:false}
        }
        
    }
    async updateUser(data:CreateUserDto, id:string): Promise<{}>{

        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("id --> ", id);
        console.log("data --> ", data);

        try{
            if(id){
                const user = await this.userModel.findById(id);
                if(user){
                    console.log("USERRES --->", user)
                    if('status' in data){ user.status = data.status}
                    if('email' in data){ user.email = data.email}
                    if('password' in data){user.password = data.password}
                    if('name' in data){user.name = data.name}
                    if('type' in data){user.type = data.type}
                    if('phone' in data){ user.phone = data.phone}
                    if('employeeCode' in data){user.employeeCode = data.employeeCode}
                    if('bussinesUnityId' in data){user.bussinesUnityId = data.bussinesUnityId}
                    console.log("result EDITED", user)
                    const response = await user.save();
                    if(response){
                        return {respuesta:true, data:response}
                    }else{
                        return {respuesta:false, error:'No se pudo actualizar el usuario'}
                    }
                }else{
                    return {respuesta:false, error:'Usuario no existente.'}
                }
            }
        }catch(err){
            return {respuesta:false, error:'Error inesperado. [2000]' + err}
        }
    }

}
